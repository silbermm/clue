defmodule Clue do
  @moduledoc """
  Documentation for Clue.
  """

  alias Clue.Boundary.GameSession

  @doc "Builds the game to prepre for play"
  def build_game(id) do
    GameSession.start_link(id)
  end

  @doc "Marks the game as ready for play"
  def start_game(game) do
  end

  @doc "Add a player to a given game"
  def add_player(game, player) do
  end

  @doc "Find a game by ID"
  def find_game(id) do
  end
end
