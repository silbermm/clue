defmodule Clue.Core.Game do
  @moduledoc ~s(Describes a game and its actions)

  alias __MODULE__
  alias Clue.Core.{Character, Deck, Player}

  # modes of game
  # :setup       - waiting for players to pick characters 
  # :ready       - players have characters and cards can be dealt - no more players can join
  # :in-progress - game is in progress 
  # :done        - game over

  @type t() :: %Game{
          players: [Player.t()],
          deck: Deck.t(),
          available_characters: [Character.t()],
          mode: :setup | :ready | :in_progress | :done
        }

  defstruct [:players, :deck, :available_characters, :mode]

  @spec new(Deck.t(), [Character.t()]) :: t()
  def new(%Deck{} = deck, characters) do
    %Game{deck: deck, available_characters: characters, mode: :setup, players: []}
  end

  @spec character_available?(Game.t(), Character.t()) :: boolean
  def character_available?(%Game{available_characters: available_characters}, %Character{
        name: name
      }) do
    Enum.any?(available_characters, &(&1.name == name))
  end

  @spec choose_character(Game.t(), Player.t(), Character.t()) :: t()
  def choose_character(game, player, character) do
    player_index = Enum.find_index(game.players, &(&1.id == player.id))

    updated_players =
      List.update_at(game.players, player_index, &Player.add_character(&1, character))

    updated_available = Enum.reject(game.available_characters, &(&1.name == character.name))
    %{game | players: updated_players, available_characters: updated_available}
  end
end
