defmodule Clue.Core.Weapon do
  @moduledoc ~s(
    Describes a weapon in the game
  )

  alias __MODULE__

  @type t() :: %Weapon{
          name: :knife | :rope | :candlestick | :revolver | :lead_pipe | :wrench
        }

  defstruct [:name]

  def all() do
    [
      %Weapon{name: :knife},
      %Weapon{name: :rope},
      %Weapon{name: :candlestick},
      %Weapon{name: :revolver},
      %Weapon{name: :lead_pipe},
      %Weapon{name: :wrench}
    ]
  end
end
