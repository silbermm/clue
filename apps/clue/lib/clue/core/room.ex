defmodule Clue.Core.Room do
  @moduledoc ~s(
    Describes a room, which is just a name, in the game
  )
  alias __MODULE__

  @type t() :: %Room{
          name:
            :hall
            | :lounge
            | :dining_room
            | :kitchen
            | :ball_room
            | :conservatory
            | :billiard_room
            | :library
            | :study
        }

  defstruct [:name]

  def all() do
    [
      %Room{name: :hall},
      %Room{name: :lounge},
      %Room{name: :dining_room},
      %Room{name: :kitchen},
      %Room{name: :ball_room},
      %Room{name: :conservatory},
      %Room{name: :billiard_room},
      %Room{name: :study}
    ]
  end
end
