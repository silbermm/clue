defmodule Clue.Core.ClueSheet do
  @moduledoc ~s(
    Provides a way for players to manage their clues
  )

  alias __MODULE__
  alias Clue.Core.{Weapon, Character, Room}

  @type t() :: %ClueSheet{
          weapons: [%{entity: Weapon.t(), data: any()}],
          characters: [%{entity: Character.t(), data: any()}],
          rooms: [%{entity: Room.t(), data: any()}]
        }

  defstruct [:characters, :weapons, :rooms]

  def new() do
    weapons = Weapon.all()
    rooms = Room.all()
    characters = Character.all()

    %ClueSheet{
      weapons: Enum.map(weapons, &%{entity: &1, data: nil}),
      rooms: Enum.map(rooms, &%{entity: &1, data: nil}),
      characters: Enum.map(characters, &%{entity: &1, data: nil})
    }
  end
end
