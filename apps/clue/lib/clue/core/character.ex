defmodule Clue.Core.Character do
  @moduledoc ~s(
    Describes a charactor, name and color,  in the game
  )

  alias __MODULE__

  @type t() :: %Character{
          name: String.t(),
          color: :white | :plum | :green | :scarlet | :mustard | :blue
        }

  defstruct [:name, :color]

  def all() do
    [
      %Character{name: "Mrs. White", color: :white},
      %Character{name: "Professor Plum", color: :plum},
      %Character{name: "Mr. Green", color: :green},
      %Character{name: "Mrs. Scarlett", color: :scarlet},
      %Character{name: "Colonel Mustard", color: :mustard},
      %Character{name: "Mrs. Peacock", color: :blue}
    ]
  end
end
