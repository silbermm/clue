defmodule Clue.Core.Player do
  @moduledoc ~s(
    Describes a player 
    )

  alias __MODULE__
  alias Clue.Core.{Character, Room, Weapon, ClueSheet}

  @type t() :: %Player{
          id: String.t(),
          name: String.t(),
          character: Character.t() | nil,
          hand: [Room.t() | Weapon.t() | Character.t()] | nil,
          clue_sheet: ClueSheet.t()
        }

  defstruct [:id, :name, :character, :hand, :clue_sheet]

  @spec new(String.t(), String.t()) :: t()
  def new(id, name) do
    %Player{id: id, name: name, clue_sheet: ClueSheet.new()}
  end

  @spec new(String.t(), String.t(), Character.t()) :: t()
  def new(id, name, %Character{} = character) do
    %Player{id: id, name: name, character: character, clue_sheet: ClueSheet.new()}
  end

  @spec add_character(t(), Character.t()) :: t()
  def add_character(%Player{} = player, %Character{} = character) do
    %{player | character: character}
  end

  @spec has_character?(Player.t()) :: boolean
  def has_character?(%Player{character: %Character{name: _}}), do: true
  def has_character?(%Player{character: nil}), do: false
  def has_character?(%Player{}), do: false

  @spec deal(t(), [Room.t() | Weapon.t() | Character.t()]) :: t()
  def deal(%Player{} = player, hand) when is_list(hand) do
    %{player | hand: hand}
  end

  @spec deal(t(), Room.t() | Weapon.t() | Character.t()) :: t()
  def deal(%Player{hand: hand} = player, card) do
    %{player | hand: [card | hand]}
  end
end
