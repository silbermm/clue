defmodule Clue.Core.Deck do
  @moduledoc "Deck"

  alias __MODULE__
  alias Clue.Core.{Character, Room, Weapon}

  @type t() :: %Deck{
          weapons: [Weapon.t()],
          characters: [Character.t()],
          rooms: [Room.t()],
          blind: %{weapon: Weapon.t(), character: Character.t(), room: Room.t()},
          shuffled: [Room.t() | Weapon.t() | Character.t()]
        }

  defstruct [:weapons, :characters, :rooms, :blind, :shuffled]

  def new() do
    %Deck{
      weapons: Weapon.all(),
      characters: Character.all(),
      rooms: Room.all()
    }
  end

  def build_blind(%Deck{weapons: w, characters: c, rooms: r} = deck) do
    random_weapon = Enum.random(w)
    random_char = Enum.random(c)
    random_room = Enum.random(r)

    %{
      deck
      | weapons: without(w, random_weapon),
        characters: without(c, random_char),
        rooms: without(r, random_room),
        blind: %{weapon: random_weapon, character: random_char, rooms: random_room}
    }
  end

  def shuffle(%Deck{weapons: w, characters: c, rooms: r} = deck) do
    all = w ++ c ++ r
    %{deck | shuffled: Enum.shuffle(all)}
  end

  defp without(lst, card), do: Enum.reject(lst, &(&1.name == card.name))
end
