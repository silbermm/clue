defmodule Clue.Boundary.PlayerSession do
  use GenServer

  alias __MODULE__

  defstruct [:player, :turn]

  def start_link(player) do
    GenServer.start_link(PlayerSession, player)
  end

  def init(player) do
    {:ok, %PlayerSession{player: player}}
  end
end
