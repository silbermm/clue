defmodule Clue.Boundary.DeckManager do
  @moduledoc "Create and deals deck"

  alias Clue.Core.{Deck, Player}

  @spec create_deck() :: Deck.t()
  def create_deck() do
    Deck.new()
    |> Deck.build_blind()
    |> Deck.shuffle()
  end

  @spec deal(Deck.t(), [Player.t()]) ::
          {:ok, [Player.t()]} | {:error, :not_shuffled | :invalid_deck}
  def deal(%Deck{shuffled: nil}, _players), do: {:error, :not_shuffled}

  def deal(%Deck{shuffled: shuffled}, players) do
    # evenly distribute shuffled cards to the players
    {hand_size, left_over_size} = determine_card_counts(shuffled, players)
    hands = build_hands(shuffled, hand_size)

    {:ok,
     players
     |> distribute_hands(hands)
     |> distribute_extras(hands, left_over_size)}
  end

  def deal(_deck, _players), do: {:error, :invalid_deck}

  defp determine_card_counts(cards, players) do
    deck_size = Enum.count(cards)
    players_size = Enum.count(players)
    {div(deck_size, players_size), rem(deck_size, players_size)}
  end

  defp build_hands(cards, hand_size), do: Enum.chunk_every(cards, hand_size)

  defp distribute_hands(players, hands) do
    result =
      Enum.reduce(players, %{players: [], hands: hands}, fn player, acc ->
        [cards | leftover] = acc.hands
        player = Player.deal(player, cards)
        %{acc | players: [player | acc.players], hands: leftover}
      end)

    result.players
  end

  defp distribute_extras(players, _hands, left_over_size) when left_over_size == 0, do: players

  defp distribute_extras(players, hands, _left_over_size) do
    leftovers = List.last(hands)

    result =
      Enum.reduce(players, %{players: [], leftovers: leftovers}, fn player, acc ->
        if Enum.count(acc.leftovers) > 0 do
          [first | rest] = acc.leftovers
          player = Player.deal(player, first)
          players = [player | acc.players]
          %{acc | players: players, leftovers: rest}
        else
          players = [player | acc.players]
          %{acc | players: players}
        end
      end)

    result.players
  end
end
