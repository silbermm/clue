defmodule Clue.Boundary.GameSession do
  @moduledoc "Manage a singular game"

  use GenServer

  alias __MODULE__
  alias Clue.Boundary.{PlayerSession, DeckManager}
  alias Clue.Core.{Game, Player, Character}

  defstruct [:players, :deck, :game_board, :available_characters, :mode]

  def start_link(id) do
    GenServer.start_link(GameSession, :ok, name: game_name(id))
  end

  def init(:ok) do
    deck = DeckManager.create_deck()
    characters = Character.all()
    {:ok, Game.new(deck, characters)}
  end

  def add_player(session, %Player{} = player) do
    GenServer.call(session, {:add_player, player})
  end

  def choose_character(session, %Player{} = player, %Character{} = character) do
    GenServer.call(session, {:choose_character, player, character})
  end

  def deal(session) do
    GenServer.call(session, :deal)
  end

  def handle_call(:deal, _from, %Game{mode: :setup} = game) do
    enough_players = Enum.count(game.players) > 2
    all_characters = Enum.all?(game.players, &Player.has_character?/1)

    if enough_players && all_characters do
      case DeckManager.deal(game.deck, game.players) do
        {:ok, players} -> {:reply, {:ok, players}, %{game | players: players, mode: :in_progress}}
        err -> {:reply, err, game}
      end
    else
      {:reply, {:error, :all_players_need_characters}, game}
    end
  end

  def handle_call({:choose_character, player, character}, _from, game) do
    # is character still available?
    if Game.character_available?(game, character) do
      game = Game.choose_character(game, player, character)
      {:reply, {:ok, Enum.find(game.players, &(&1.id == player.id))}, game}
    else
      {:reply, {:error, :not_available}, game}
    end
  end

  def handle_call({:add_player, player}, _from, %Game{mode: :setup} = game) do
    # should I create another process here for the player session??
    # player_pid = PlayerSession.start_link(player)

    {:reply, {:ok, player}, %{game | players: [player | game.players]}}
  end

  def handle_call({:add_player, _player}, _from, game) do
    # should I create another process here for the player session??
    # player_pid = PlayerSession.start_link(player)

    {:reply, {:error, :invalid_game_mode}, game}
  end

  defp game_name(id) do
    {:via, Registry, {GameRegistry, id}}
  end
end
