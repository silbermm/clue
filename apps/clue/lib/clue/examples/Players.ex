defmodule Clue.Examples.Players do
  @moduledoc "Generate Example Players"

  alias Clue.Core.{Player, Character}

  @doc "Creates 4 new players"
  def generate_players() do
    available_characters = Character.all()

    [
      Player.new("123-456-789", "player1", Enum.at(available_characters, 2)),
      Player.new("123-456-987", "player2", Enum.at(available_characters, 1)),
      Player.new("123-456-798", "player3", Enum.at(available_characters, 3)),
      Player.new("123-456-654", "player4", Enum.at(available_characters, 0))
    ]
  end
end
