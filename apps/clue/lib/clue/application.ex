defmodule Clue.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      {Registry, keys: :unique, name: GameRegistry}
    ]

    opts = [strategy: :one_for_one, name: ClueCore.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
